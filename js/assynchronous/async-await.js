//------------ EXAMPLE 1 ---------------//

const dadosExemplo = fetch("/js/assynchronous/files/example.json");

dadosExemplo.then(response => {
    
    const dadosExemploJSON = response.json();
    
    //response.headers.forEach(console.log);
    
    return dadosExemploJSON;
    
})
.then(responseJSON => {
    console.log(responseJSON);
})

//-------------- EXAMPLE 2 ------------ //

console.log("Começo da função!");

async function getExemplo2() {
    
    const corpoDaRequisicao = new URLSearchParams({
        'userName': 'test@gmail.com',
        'password': 'Password!',
        'grant_type': 'password'
    });
    
    const dataRequisicao = {
        method: "POST",
        body: corpoDaRequisicao
    };
    
    /**
    * O PHP aceita posts somente no formato "xxx-form-urlencoded", ou seja, transformados para query params de URL
    */
    const dadosExemplo2 = await fetch("/ajax/ajax.php", dataRequisicao);
    
    const dadosExemplo2JSON = await dadosExemplo2.text();
    
    console.log(dadosExemplo2JSON);
    
    const dadosExemplo2Array = JSON.parse(dadosExemplo2JSON);
    
    console.log(dadosExemplo2Array);
    
}

getExemplo2();

/**
* Ele passa aqui primeiro antes de partir para os próximos passos da função assíncrona.
*/
console.log("Final da função!");




// ------------ EXAMPLE 3 --------------- //

/**
* REPRODUZINDO O COMPORTAMENTO DO VUE.JS
* 
* No Script abaixo, realizaremos a chamada assíncrona de diferentes páginas Web, sem a necessidade de efetuar o carregamento completo do site
*/


const linksDaPagina = document.querySelectorAll("a");

linksDaPagina.forEach(link => {
    
    link.addEventListener('click', (event) => {
        event.preventDefault();
        
        carregamentoAssincrono(event.target.href)
    })
    
})

/**
 * Função responsável por realizar o carregamento assícrono dos elementos de uma página
 * @param {String} hrefElement 
 */
async function carregamentoAssincrono(hrefElement) {
    let pageContent = await fetch(hrefElement);
        
    let pageContentText = await pageContent.text();

    let parser = new DOMParser();

    let pageContentHTML = parser.parseFromString(pageContentText, 'text/html');

    document.querySelector(".maincontent").innerHTML = pageContentHTML.querySelector(".maincontent").innerHTML;

    document.title = pageContentHTML.title;

    window.history.pushState(null, null, hrefElement);
}

/**
 * Manipula evento de clique nos botões de "Voltar" e "Próximo"
 */
window.addEventListener("popstate", () => {
    carregamentoAssincrono(window.location.pathname)
});

