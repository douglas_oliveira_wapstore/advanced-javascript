const numeroAleatorio = 6;

// -------------- DECLARAÇÃO DE PROMISES ----------------- //

let promessa1 = new Promise(function (resolve, reject) {

    if(numeroAleatorio >= 5) {

        setTimeout(function () {
            resolve("[PROMISE1] Testando sommmmmmmmmmmmmmm!")
        }, 2000);

    }
    else {

        setTimeout(() => reject(Error("[PROMISE1] Deu bom não amigo")), 2000);

    }

});




let promessa2 = new Promise(function (resolve, reject) {
    
    if(numeroAleatorio < 5) {

        setTimeout(() => {
            resolve("[PROMISE2] Deu bom de novo amigo")
        },5000)

    }

    else {

        setTimeout(() => {
            reject(Error("[PROMISE2] Deu ruim de novo carinha!"));
        },5000)

    }

})

let promessa3 = Promise.all([promessa1, promessa2]);

let promessa4 = Promise.race([promessa1, promessa2]);

// ---------------- PROCESSAMENTO DE PROMISES ------------- //

/**
 * Esta função será executada apenas quando a promessa for resolvida. O retorno da promise estará na variável resolvemsg
 */
promessa1.then(function (resolvemsg) {
    console.log(resolvemsg);
})


promessa2.then(function (resolvemsg) {
    console.log("[PROMISE2] Eu de novoo!");
    console.log(resolvemsg);
})

/**
 * Esta função será executada apenas quando a promessa for rejeitada. O retorno de erro estará na variável rejectmsg
 */
.catch(function (rejectmsg) {
    console.log("[PROMISE2] Quando a promise é rejeitada, ele passa somente pela função catch(), a não ser que haja ump finally.");
    console.log(rejectmsg);
})

/**
 * Esta função será executada independentemente da função ter sido rejeitada ou finalizada
 */
.finally(function () {
    console.log("[PROMISE2] Porém, ele passa aqui de qualquer jeito!");
})

/**
 * Esta função será executada quando ambas as promisses forem executadas e retornadas com sucesso
 */
promessa3.then(function (resolvemsg) {
    console.log("[PROMISE3] Aqui ele só chega quando ele resolver as duas promises criadas!");
    console.log(resolvemsg);
})
.catch(function (rejectmsg) {
    console.log("[PROMISE3] Aqui ele só chega quando pelo menos uma das promises forem recusadas");
    console.log("FROM [PROMISE3]:" + rejectmsg);
})

/**
 * Essa função processará a promise que for processada primeiro
 */
promessa4.then(function (resolvemsg) {
    console.log("[PROMISE4] Aqui ele só chega quando a primeira promise do array for processada.");
    console.log("FROM [PROMISE4]: " + resolvemsg);
})

