const promise = new Promise((resolve, reject) => {

    const cliqueMeButton = document.querySelector(".button-animais-clique-me");

    cliqueMeButton.addEventListener("click", () => {

        let numeroAleatorio = Math.random();

        if(numeroAleatorio >= 0.5) resolve("a) O número é maior ou igual a 5");
        
        reject(Error("a) O número é menor que 5"));

    })

});

const promise2 = new Promise((resolve, reject) => {

    const cliqueMeButton = document.querySelector(".button-animais-clique-me");

    cliqueMeButton.addEventListener("click", () => {

        let numeroAleatorio = Math.random();

        if(numeroAleatorio >= 0.5) resolve("b) O número é maior ou igual a 5");
        
        reject(Error("b) O número é menor que 5"));

    })

});

const todosOSNumerosSaoMaioresQue5 = Promise.all([promise, promise2]);

const quemChegouPrimeiro = Promise.race([promise, promise2]);

//PROMESSAS SÃO CUMPRIDAS APENAS UMA VEZ
promise.then((reason) => {
    window.alert(reason);
})

//CASO A PROMESSA TENHA SIDO REJEITADA
promise.catch((reason) => {
    console.log("Deu erro mermão: " + reason);
});

promise2.then((reason) => {
    window.alert(reason);
})

//CASO A PROMESSA TENHA SIDO REJEITADA
promise2.catch((reason) => {
    console.log("Deu erro mermão: " + reason);
});

//ESSE CARINHA SÓ SERÁ EXECUTADO CASO TODAS AS RESPOSTAS TENHAM SIDO ACEITAS
todosOSNumerosSaoMaioresQue5.then((response) => {
    console.log("As responstas são: " + response);
})

todosOSNumerosSaoMaioresQue5.catch((response) => {
    console.log("Nem todas as respostas deram bom :/");
    console.log(response);
})

quemChegouPrimeiro.then((response) => {
    console.log("Quem chegou primeiro? " + response);
})