/******************************************/
/*********** REQUISIÇÃO GET ***************/
/******************************************/

const informacoesPessoa = fetch("./js/assynchronous/files/example.json");
const informacoesPessoa2 = fetch("./js/assynchronous/files/example.json");

const imagemAleatoria = fetch("./js/assynchronous/files/example.gif");

//CONSUMINDO DADOS NO FORMATO JSON
informacoesPessoa.then(function (response) {
    return response.json();
})
.then(function(responseJson) {
    console.log(responseJson);
})

//CONSUMINDO DADOS NO FORMATO TEXT
informacoesPessoa2.then(function (response) {
    return response.text();
})
.then(function (responseTxt) {
    console.log(responseTxt);
})

//MONTANDO IMAGEM OBTIDA PELA PROMISSE
imagemAleatoria.then(function (response) {
    return response.blob();
})
.then(function (responseBlob) {
    const blobUrl = URL.createObjectURL(responseBlob);

    const imgElement = document.createElement("img");

    imgElement.setAttribute("src", blobUrl);

    const footer = document.querySelector("footer");

    footer.appendChild(imgElement);
})

/******************************************/
/*********** REQUISIÇÃO POST **************/
/******************************************/

//ASSIM NÃO FUNCIONA
const post = {
    nome: "douglinasfasdifbblkjb",
    idade: "fajdbkajsdbflksajfbasdklçjf",
    gostaDe: "fahbkjbwlkjfbwlaufbliuabsdf",
    douglinhas: "aAAAAAAAaaaaaaaaaaaaaa",
    viviane: "asfksdjfalkjdsflkdjsfl",
    naoGos: "aushdfuabewkljfbskja",
    heart: "aaaaaaaaaaaaaaa"
};

const options = {
    method: "POST",
    body: JSON.stringify(post),
    headers: {
        "Content-Type" : "application/x-www-form-urlencoded; charset=utf-8"
    }
}

const formularioAleatorio = fetch("./ajax/ajax.php", options);

formularioAleatorio.then(response => response.json())
.then(function (responseJson) {
    console.log(responseJson);
})

console.log("testando se esse log será disparado antes do script php finalizar sua execução");