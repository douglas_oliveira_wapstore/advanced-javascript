/**
 * Cria um novo animal
 * @param {String} nome Nome do animal
 * @param {String} tipo Tipo do animal
 * @param {String} barulho Barulho realizado pelo animal
 */

function Animal(nome, tipo, som) {
    this.nome  = nome;
    this.tipo  = tipo;
    this.som   = som;
}

Animal.prototype.barulho = function() {
    
    const somBarulho = this.nome + " fez: " + this.som;
    return somBarulho;

}

var obAnimal = new Animal("Clotilde", "Vaca", "Muuuuuuuuuu");

console.log(obAnimal.barulho())

//--------------- CLASSES COM JAVASCRIPT ------------ //

class Instrumento {
    constructor(nome, clave, tipo) {
        this._nome  = nome;
        this._clave = clave;
        this._tipo  = tipo;
    }
    /**
     * Os getters e setters serõo acessados assim que, respectivamente, os valores das propriedadas forem acessadas e setadas
     */
    set musica(music) {
        this._musica = " [DOUGLAS] " + music;
    }
    get musica() {
        return "O nome da música é: " + this._musica
    }
    getInstrument() {

        let frase = this._nome + " está na clave de " + this._clave + " e é do tipo " + this._tipo + ".";

        return frase;

    }
    getName() {
        return this._nome;
    }
    getClave() {
        return this._clave;
    }
    getTipo() {
        return this._tipo;
    }
}

const obInstrumento = new Instrumento("Teclado", "Sol/Fá", "Teclas");

console.log("Nome: " + obInstrumento.getName());
console.log("Clave: " + obInstrumento.getClave());
console.log("Tipo: " + obInstrumento.getTipo());

obInstrumento.musica = "Sinfonia de Beethoven";

console.log(obInstrumento.musica);

//----------- HERANÇA COM JAVASCRIPT --------//

class InstrumentoSopro extends Instrumento{

    constructor(nome, clave, tipo, oitava) {
        super(nome, clave, tipo);
        this._oitava = oitava;
    }
    getName() {
        return "O nome desse instrumento de sopro é: " + super.getName();
    }
    assoprar() {
        return "ffffffffffffffffffffffff";
    }

}

const instrumentoSopro = new InstrumentoSopro("Flauta", "Sol", "Sopro", "Soprano");


console.log(instrumentoSopro.getName());
console.log(instrumentoSopro.assoprar());