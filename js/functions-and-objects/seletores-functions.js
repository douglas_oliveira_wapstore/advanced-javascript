const animais = document.getElementById('animais');
const gridSection = document.getElementsByClassName('grid-section');
const ul = document.getElementsByTagName('ul');


// querySelector retorna o primeiro elemento que combinar com o seu seletor CSS

const primeiraLi = document.querySelector('li');
// console.log(primeiraLi);

const primeiraUl = document.querySelector('ul');
// console.log(primeiraUl);

const animaisImg = document.querySelectorAll('.animais img');
// console.log(animaisImg);

//--------------------------------------------------------------------------------//

//RETURNA UMA HTML COLLECTION
const gridSectionHTML = document.getElementsByClassName('grid-section');

//RETORNA UMA NODELIST
const gridSectionNode = document.querySelectorAll('.grid-section');

/**
 * As HTML Collections se atualizam conforme novos elementos vão se adequando às regras do seletor
 * Não são percorríveis
 * 
 * Já as Nodelists permanecem estáticas independentemente de novos elementos se adequarem
 * São percorríveis
 */

 gridSectionNode.forEach((item, index, elementoQueChamouOFor) => {
    // console.log(item);
    // console.log(index);
    // console.log(elementoQueChamouOFor);
});

const arrayGrid = Array.from(gridSectionHTML);

arrayGrid.forEach(function (item) {
    // console.log(item);
})


// -------------------------------------------- FOR EACH -----------------------------------,
const imgs = document.querySelectorAll('img');

imgs.forEach(function(item, index) {
    console.log(item);
});


/**
 * forEach é um método de Array, alguns objetos array-like possuem este método. Caso não possua, o ideal é transformá-lo em um array
 * 
 * Array-like(NodeList)
 */



/**
 * Arrow function -Sintaxe curta em relaçõa a function expression
 */

imgs.forEach((item, index) => {
    console.log('arrooooooooooooow');
    console.log(item);
});

imgs.forEach(() => console.log('sdfblfkdjbklsfjdb'));