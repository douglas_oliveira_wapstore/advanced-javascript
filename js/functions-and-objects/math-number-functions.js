const isNan = Number.isNaN(NaN);
console.log("Não é um número? " + isNan); //true

const isANumber = Number.isNaN(4 + 5);
console.log("Não é um número? " + isANumber); //false

let isInteger = Number.isInteger(9);
console.log("É inteiro? " + isInteger); //true

isInteger = Number.isInteger(9.5);
console.log("É inteiro 9.5? " + isInteger); //false


//CONVERSÕES
console.log(parseFloat("987654.50")); //987654.5
console.log(parseFloat("98,7654.50")); //98
console.log(parseFloat("98,76a54.d50")); //98
console.log(parseFloat("9u8,76a54.d50")); //9
console.log(parseFloat("p9u8,76a54.d50")); //NaN

console.log(parseInt("1165165161")); // 1165165161
console.log(parseInt("116516a5161")); //116516
console.log(parseInt("11p6516a5161")); //11
console.log(parseInt("11,51651.9")); //11
console.log(parseInt("11021.5959654",2)); //6

//CASO UM NÚMERO INVÁLIDO SEJA ENCONTRADO NO MEIO DA EXPRESSÃO, APENAS OS NÚMEROS À SUA ESQUERDA SERÃO CONSIDERADOS PARA CÁLCULO. NO NÚMERO ACIMA, APENAS O 110 FOI CONSIDERADO NA BASE BINÁRIA

console.log(parseInt("FFF",16)); //BASE HEXADECIMAL
console.log(parseInt("771",8));  //BASE OCTAL

const number1 = 123456.1589486516;
const number2 = 123456.1519486516;
const number3 = 10;

console.log(number1.toFixed(2)); //123456.16
console.log(number2.toFixed(2)); //123456.15

console.log(number3.toString(16)); // a

let convertedNumber = number1.toLocaleString('en-US', {style: 'currency', currency: 'BRL'});

console.log(convertedNumber);

convertedNumber = number1.toLocaleString('pt-BR', {style: 'currency', currency: 'USD'});

console.log(convertedNumber);

/**
 * FUNÇÕES MATEMÁTICAS
 */


console.log(Math.ceil(1551651.98)); //1551652
console.log(Math.floor(1551651.98)); //1551651
console.log(Math.abs(-1551651.98)); // 1551651.98
console.log(Math.round(1551651.98)); // 1551652
console.log(Math.round(1551651.5)); // 1551652

console.log(Math.max(1,2,3,4,10,40,50,100,5)); //100
console.log(Math.min(1,2,3,4,10,40,50,100,5)); //1

//PADRÃO PARA RETORNAR NÚMEROS EM UM DETERMINADO INTERVALO
let max = 45;
let min = 40;

const calcRandom = Math.floor(Math.random() * (max - min + 1) + min);

console.log(calcRandom);
