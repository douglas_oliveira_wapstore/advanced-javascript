const menu = document.querySelector('.menu');

menu.className += ' vermelho';



const animais = document.querySelector('.animais');

console.log(animais.attributes);


const img = document.querySelector('img');

console.log(img);

console.log(img.getAttribute('src'));

/**
 * EXISTEM PROPRIEDADES QUE NÃO PERMITEM A MUDANÇA DE SEUS VALORES. ESSAS SÃO CONSIDERADAS READ ONLY. AS QUE PERMITEM A MUDANÇA SÃO CHAMADAS DE WRITABLE
 * 
 * A MAIORIA DOS ATRIBUTOS SÃO READ ONLY, PERMITINDO A MUDANÇA DE SEUS VALORES APENAS POR MÉTODOS
 */