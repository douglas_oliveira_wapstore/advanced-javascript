const string1 = "douglas";

console.log(string1);

const string1length = string1.length;

console.log("Tamanho do texto: " + string1length);

console.log("Primeiro caractere do texto: " + string1[0]);
console.log("Primeiro caractere do texto utilizando chatAt(): " + string1.charAt(0));
console.log("Último caractere do texto utilizando chatAt(): " + string1.charAt(string1.length - 1));

const stringFinal = string1.concat(" oliveira paschoal");

console.log("Meu nome completo: " + stringFinal);

/** **************** TESTANDO INCLUDES() ****************** */

const listaDeNomes  = '"carlinhos", "maria", "josé", "douglas"';

console.log("lista de nomes contém 'douglas'? " + listaDeNomes.includes(string1));
console.log("douglas contém 'lista de nomes'? " + string1.includes(listaDeNomes));

console.log("douglas começa  com 'do' " + string1.startsWith("do", 0));
console.log("douglas termina com 'as' " + string1.endsWith("as"));

console.log("cortando parte do nome: " + stringFinal.slice(2,6));
console.log("cortando parte do nome ao contrário: " + stringFinal.slice(-3));

console.log("preenchendo o nome com pontos: " + stringFinal.padEnd(100, '.asdfg'));
console.log("preenchendo o nome com pontos: " + stringFinal.padStart(100, '.asdfg'));