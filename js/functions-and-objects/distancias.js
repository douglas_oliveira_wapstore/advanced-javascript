const listaAnimais = document.querySelector('.animais-lista');

console.log("Elemento", listaAnimais);

console.log("clientHeight (Height + Padding)", listaAnimais.clientHeight);
console.log("offsetHeight (Height + Padding + Border)", listaAnimais.offsetHeight);

window.onload = ((param) => {
    console.log("scrollHeight (Height + Padding + Border + Scroll)", listaAnimais.scrollHeight);
})

console.log("offsetTop (Distância do topo do elemento para o topo da página)", listaAnimais.offsetTop);
console.log("offsetLeft (Distância entre o canto esquerdo do elemento para o canto esquerdo da página)", listaAnimais.offsetLeft);

console.log("GetBoundingClientRect()", listaAnimais.getBoundingClientRect());

console.log("window.innerWidth (Largura interna da página)", window.innerWidth);
console.log("window.outerWidth (Largura interna + tamanho da barra de pesquisa)", window.outerWidth);
console.log("window.innerHeight (Altura interna da página)", window.innerHeight);
console.log("window.outerHeight (Altura interna + externa)", window.outerHeight);
console.log("window.pageXOffset", window.pageXOffset);
console.log("window.pageYOffset", window.pageYOffset);
