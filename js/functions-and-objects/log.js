/**
 * DOM é uma interface que representa um documento HTML e XML através de objetos
 */

console.log(window);

/**
 * window possui um conjunto de métodos, atributos e propriedades
 */

console.log(window.location.href);

//alert('douglinhas');

const elementoAtivo = document.querySelector('.ativo');

console.log(elementoAtivo);

const linguagem = window.navigator.language;

console.log(linguagem);

