/**
 * MutationObserver fornece aos desenvolvedores uma maneira de reagir a mudanças em um DOM
 */

const botaoCliqueAqui = document.querySelector(".button-animais-clique-me");

botaoCliqueAqui.addEventListener("click", function (e) {
    e.target.classList.toggle("douglinhas");
})

//ATIVA O EVENTO APENAS UMA VEZ E SE DESCONECTA DO OBSERVADOR
function handleCLickBotaoCliqueAqui(mutation) {
    clickBotaoCliqueAquiObserver.disconnect();
}

//INICIALIZA UM OBSERVADOR DE MUTAÇÕES OCORRDIAS NO DOM
// QUANDO ESSAS MUTAÇÕES OCORREREM,A FUNÇÃO HANDLECLICK SERÁ CHAMADA
const clickBotaoCliqueAquiObserver = new MutationObserver(handleCLickBotaoCliqueAqui);

//INICIALIZA A OBSERVAÇÃO DO OBJETO DOM, VERIFICANDO SE ALGUM DOS SEUS ATRIBUTOS FORAM ALTERADOS
clickBotaoCliqueAquiObserver.observe(botaoCliqueAqui, {attributes: true})
