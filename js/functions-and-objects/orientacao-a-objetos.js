function Pessoa(nome, idade) {
    this.nome = nome;
    this.idade = idade;

    this.andar = function() {
        return this.nome + " andou!";
    }

    this.peidar = function () {
        return this.nome + " peidou!";
    }

    this.abracar = function () {
        return this.nome + " abraçou e tem " + this.idade + " anos!";
    }

}

const obPessoa = new Pessoa("Douglinhas76", 20);

const andar = obPessoa.andar();

console.log(andar);
console.log(obPessoa.andar);

console.log(obPessoa);