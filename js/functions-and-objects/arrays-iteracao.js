let comidas = ['arroz', 'feijão', 'macarrão', 'batata', 'macarrão'];

comidas.forEach((item, index, array) => {
    console.log(item.toUpperCase(),index,array);
    array.unshift(item);
    //ALTERAR O ARRAY PASSADO COMO PARÂMETRO NA FUNÇÃO CALLBACK TBM ALTERA O ARRAY ORIGINAL
})

console.log("array final", comidas);

//INSERINDO O PREFIXO "COMIDA" NO INÍCIO DE CADA ITEM DO ARRAY
comidas = comidas.map((item, index, array) => {
    return "Comida " + item;
})

console.log("Array após o map()", comidas);

//FUNÇÃO CUMULATIVA

const numeros = [8,920,8.5,2,93,74,89,45,112,8745,54,65,23];

numeros.reduce(function (valorAnterior, valorAtual, arrayCompleto) {
    console.log(valorAnterior, valorAtual, arrayCompleto);

    return valorAnterior + valorAtual;
},150)

/**
    EXEMPLO DE LÓGICA
    (CASO O SEGUNDO PARAMETRO NÃO FOSSE 150)

    8 920 1 
    928 8.5 2 
    936.5 2 3 
    938.5 93 4 
    1031.5 74 5 
    1105.5 89 6 
    1194.5 45 7 
    1239.5 112 8 
    1351.5 8745 9 
    10096.5 54 10 
    10150.5 65 11 
    10215.5 23 12

 */

console.log('-------------------------------------------');

//MESMA COISA, SÓ QUE INVERTIDO
numeros.reduceRight(function (valorAnterior, valorAtual, arrayCompleto) {
    console.log(valorAnterior, valorAtual, arrayCompleto);

    return valorAnterior + valorAtual;
},150)


const existeAlgumNumeroMaiorQue100 = numeros.some((item) => item > 10000)

console.log("Existe algum número maior que 10000", existeAlgumNumeroMaiorQue100);

const todosOsNumerosSaoMaioresDoQue1 = numeros.every((numero) => numero > 1)

console.log("Todos os números são maiores do que 1?", todosOsNumerosSaoMaioresDoQue1);


const existeAlgumNumeroMaiorQue500 = numeros.find((numero) => numero > 500);

console.log("Existe algum número maior que 500?", existeAlgumNumeroMaiorQue500);


const existeAlgumNumeroMaiorQue600 = numeros.findIndex((numero) => numero > 600);

console.log("Existe algum número maior que 600?", existeAlgumNumeroMaiorQue600);

const somenteNumerosMaioresQue700 = numeros.filter((numero) => numero > 700);

console.log(somenteNumerosMaioresQue700);