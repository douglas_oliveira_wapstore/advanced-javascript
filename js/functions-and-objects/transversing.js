const menu = document.querySelector(".menu");

menu.outerHTML; // todo o html do elemento
menu.innerHTML; // HTML interno
menu.innerText; //texto, sem as tags

/**
 * TRANSVERSING
 * 
 * Como navegar pelo DOM, utilizando propriedades e métodos
 */

const lista = document.querySelector(".animais-lista");

lista.parentElement; //pai
lista.parentElement.parentElement; // pai do pai
lista.previousElementSibling; // elemento acima
lista.nextElementSibling; // elemento abaixo

lista.childNodes; // Filhos, incluindo comentários e enters
lista.children; //HTML Collection com os filhos
lista.children[0]; // Primeiro filho
lista.children[--lista.children.length]; // último filho

lista.querySelectorAll('li'); //todos os LI's
lista.querySelector('li:last-child'); //último filho

/**
 * ELEMENT VS. NODE
 * 
 * Element's representam um elemento HTML, ou seja, uma tag. Node representa um nó, e pode ser um elemento (Element), texto, comentário, quebra de linha e mais
 */

lista.previousElementSibling; //elemento acima
lista.previousSibling; //node acima

lista.firstChild; //primeiro node child
lista.childNodes; //todos os node child