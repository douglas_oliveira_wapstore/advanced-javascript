function Pessoa(nome, idade) {
    this.nome = nome;
    this.idade = idade;

    this.andar = function() {
        return this.nome + " andou!";
    }

    this.peidar = function () {
        return this.nome + " peidou!";
    }

    this.abracar = function () {
        return this.nome + " abraçou e tem " + this.idade + " anos!";
    }

}

Pessoa.prototype.caminhar  = function () {
    return "ele caminhou nos montes shuribibibibi";
}

const obPessoa = new Pessoa("Douglinhas76", 20);

const andar = obPessoa.andar();
