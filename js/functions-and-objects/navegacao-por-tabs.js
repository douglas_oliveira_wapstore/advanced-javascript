window.onload = function () {
    mostrarConteudoAnimaisDeAcordoComScroll();
}

let animaisLista      = document.querySelector(".animais-lista");
let imgAnimaisLista   = document.querySelectorAll(".animais-lista li");
let imgAnimaisContent = document.querySelectorAll(".animais-descricao section");

function calcularConteudoASerMostrado() {
    var indexMenorDiferencaEntreOsNegativos = 0;
    var menorDiferencaDentreOsNegativos = 0;
    
    imgAnimaisLista.forEach(function (imgAnimal, index) {

        let distanceBoxAnimaisListaToTop = animaisLista.getBoundingClientRect().top;
        let distanceImageAnimalToTop     = imgAnimal.getBoundingClientRect().top;

        let differenceBoxToAnimalImg = distanceImageAnimalToTop - distanceBoxAnimaisListaToTop;

        if(index == 0) {
             indexMenorDiferencaEntreOsNegativos = index;
             menorDiferencaDentreOsNegativos = differenceBoxToAnimalImg;
        }

        if(differenceBoxToAnimalImg <= 0 && differenceBoxToAnimalImg >= menorDiferencaDentreOsNegativos) {
            indexMenorDiferencaEntreOsNegativos = index;
            menorDiferencaDentreOsNegativos = differenceBoxToAnimalImg;
        }

    })

    return {indexMenorDiferencaEntreOsNegativos, menorDiferencaDentreOsNegativos};

}


animaisLista.addEventListener("scroll", (event) => {
    mostrarConteudoAnimaisDeAcordoComScroll();
})

function mostrarConteudoAnimaisDeAcordoComScroll(params) {

    let dadosConteudoASerMostrado =     calcularConteudoASerMostrado();

    imgAnimaisContent.forEach((animalContent) => {
        animalContent.classList.remove("ativo");
    })

    imgAnimaisContent[dadosConteudoASerMostrado.indexMenorDiferencaEntreOsNegativos].classList.add("ativo");

}