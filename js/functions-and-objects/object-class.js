const pessoa = {
    nome: "douglinhas",
    idade: 19,
    instrumento: "teclado",
    tocar() {
        return this.nome + " está tocando teclado";
    },
    peidar(volume) {
        return this.nome + " peidou " + volume;
    }
}

//equivalente ao "clone" no PHP
const pessoa2 = Object.create(pessoa);
// usar esse no lugar do pessoa2 = pessoa, pois nesse caso, pessoa2 será apenas uma referência ao objeto original, e qualquer coisa alterada em um refletirá no outro

pessoa2.observacoes = "nada a declarar";
pessoa2.profissao   = "programador";
pessoa2.nome        = "carlos";

console.log(pessoa);
console.log(pessoa2);

console.log("O nome da pessoa 1 é: " + pessoa.nome);
console.log("O nome da pessoa 2 é: " + pessoa2.nome);

//tanto pessoa3 quanto pessoa possuirão as mesmas propriedades/métodos
const pessoa3 = Object.assign(pessoa,pessoa2);

console.log(pessoa3);
console.log(pessoa);

/** 
 
    configurable
        true se e somente se o tipo deste descritor de propriedades pode ser modificada e se a propriedade pode ser apagada do objeto correspondente.
        Valor padrão é false.
    enumerable
        true se e somente se este propriedade aparece durante enumeração das propriedade sobre o objeto correspondente.
        Valor padrão é false.

    value
        O valor associado com a propriedade. Pode ser qualquer valor válido em JavaScript value (número, objeto, função, etc).
        Valor padrão é undefined.
    writable
        true se e somente se o valor associado com a propriedade pode ser modificada com um assignment operator (en-US).
        Valor padrão é false.

    get
        Uma função a qual serve com um getter para a propriedade, ou undefined se não existe getter. A retorno da função será usado como o valor da propriedade.
        Valor padrão é undefined.
    set
        Uma função a qual server com um setter para a propriedade, ou undefined se não existe setter. A função receberá como argumento somente o novo valor sendo atribuído à propriedade.
        Valor padrão é undefined.

 */

const livro = {};

Object.defineProperties(livro,
    {
        quantidadeDePaginas: {
            value: 500,
            enumerable: false,
            writable: false,
            configurable: false
        },
        titulo: {
            get() {
                return this._titulo + "Volume 2";
            },
            set(valor) {
                this._titulo = valor + " - O mistério de Hogwarts";
            }
        }
    }
);

livro.titulo = "O Rei Arthur";

console.log(livro);
