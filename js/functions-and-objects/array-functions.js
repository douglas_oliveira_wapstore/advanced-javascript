const arrayAleatorio = [
    "carro",
    "bicicleta",
    "motocicleta",
    [
        "remédio",
        "dipirona"
    ],
    function () {
        return "dougrinha";
    },
    {
        nome: 'dougrinha sanches',
        idade: 18
    }
];

//ACESSANDO VALORES
console.log(arrayAleatorio);
console.log(arrayAleatorio[0]);
console.log(arrayAleatorio[1]);
console.log(arrayAleatorio[2]);
console.log(arrayAleatorio[3][0]);
console.log(arrayAleatorio[4]());
console.log(arrayAleatorio[5].nome);

arrayAleatorio[20] = "Dogrinhaa";

console.log(arrayAleatorio);

//CONVERSÕES

const todosOsParagrafos = document.querySelectorAll("p");

const todosOsParagrafosArray = Array.from(todosOsParagrafos);

console.log(todosOsParagrafos);
console.log(todosOsParagrafosArray);

const objetoDeExemplo = {
    0: "douglinhas",
    1: "14 anos",
    2: "tocar teclado",
    3: "jogar futebol",
    length: 4
}

const objectToArray = Array.from(objetoDeExemplo);

console.log(objectToArray);

// MANEIRAS DE CRIAR ARRAYS

console.log(Array.of(2,6,7,8,9,4));
console.log(new Array(9));
console.log(new Array(9,5,7));
console.log(Array(9,5,7));

//MANIPULAÇÃO DE ARRAYS

const arrayDeExemplo = ["arroz", "douglas", "batata", "macarrão", "sabonete", "amigo"];

const arrayDeExemplo2 = [1,2,3,80,7,45,2,90,9,17,5];

console.log(arrayDeExemplo.sort());
console.log(arrayDeExemplo2.sort());

/**
 * A ordenação e valores no sort() é feita, por padrão, tendo como parâmetro o código de ponto Unicode dos caracteres inseridos
 * 
 * Por isso, a ordenação de números não é "estável"
 */

function ordenaArrayDeNumerosCrescendo(a,b) {
    return a-b;
}

console.log(arrayDeExemplo2.sort(ordenaArrayDeNumerosCrescendo)); // ORDENAÇÃO CORRETA!

function ordenaArrayDeNumerosDecrescendo(a,b) {
    return b-a;
}

console.log(arrayDeExemplo2.sort(ordenaArrayDeNumerosDecrescendo)); // ORDENAÇÃO CORRETA!

console.log(arrayDeExemplo.sort(ordenaArrayDeNumerosDecrescendo)); // ORDENAÇÃO CORRETA! ORDENA TEXTO TAMBÉM



//ADICIONANDO E REMOVENDO ELEMENTOS
const arrayDeExemplo10 = ["carro", "bicicleta", "moto", "dinheiro", "estudos"];

arrayDeExemplo10.unshift("viviane", "douglas", "gilberto");

console.log(arrayDeExemplo10);

arrayDeExemplo10.push('celular', 'bebida', 'guaraná', 'natu');

console.log(arrayDeExemplo10);

arrayDeExemplo10.shift();
arrayDeExemplo10.pop();

console.log(arrayDeExemplo10);
//[ "douglas", "gilberto", "carro", "bicicleta", "moto", "dinheiro", "estudos", "celular", "bebida", "guaraná" ]

//NO TERCEIRO ELEMENTO DO ARRAY, INSIRA OS VALORES "DOUGLAS" E "OLIVEIRA" E "PASCHOAL", EXCLUINDO SOMENTE OS ELEMENTOS QUE SERÃO OCUPADOS POR "DOUGLAS" E "OLIVEIRA"
arrayDeExemplo10.splice(2,2,"douglas","oliveira","paschoal");
//[ "douglas", "gilberto", "carro", "bicicleta", "moto", "dinheiro", "estudos", "celular", "bebida", "guaraná" ]

arrayDeExemplo10.splice(5,3,"douglas","oliveira","paschoal");
//[ "douglas", "gilberto", "douglas", "oliveira", "paschoal", "douglas", "oliveira", "paschoal", "celular", "bebida", … ]

console.log(arrayDeExemplo10);

arrayDeExemplo10.copyWithin(2,0,2);

console.log(arrayDeExemplo10);

arrayDeExemplo10.fill("Dogrinhaaa",3,8);

console.log(arrayDeExemplo10);

console.log("Array possui o elemento 'Dogrinhaaa'? " + arrayDeExemplo10.includes("Dogrinhaaa"));
console.log("Qual o index do elemento 'Dogrinhaaa'? " + arrayDeExemplo10.indexOf("Dogrinhaaa"));
console.log("Qual o último index do elemento 'Dogrinhaaa'? " + arrayDeExemplo10.lastIndexOf("Dogrinhaaa"));


//SEPARANDO ELEMENTOS EM ARRAYS E VICE-VERSA

let arrayInString = arrayDeExemplo.join(",");

console.log(arrayInString);

console.log(arrayInString.split("a"));