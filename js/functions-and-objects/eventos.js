const img = document.querySelector("img");

function callback(event) {
    console.log(event);
    //event.currentTarget    = elemento para o qual foi adicionado o evento
    //event.target           = elemento que foi clicado
    //this                   = elemento que foi clicado
    //event.type             = tipo de evento
    //event.preventDefault() = previne o comportamento default
}

img.addEventListener('click', callback); // ADICIONA UM EVENTO DE CLIQUE PARA UM ELEMENTO

function handleKeyboard(event) {
    console.log(event.key);
}

window.addEventListener('keydown', handleKeyboard);